﻿using sfs_stress_tool.framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace sfs_stress_tool.example
{
    class Program
    {
        const string CONFIG_FILE_NAME = @"..\..\..\config.config";
        static StressTestReplicator<TestClient> m_stressTestReplicator;
        static void Main(string[] a_args)
        {
            Util.WriteLog("Starting application on threadId " + Thread.CurrentThread.ManagedThreadId);
            Dictionary<string, string> l_configData = ReadConfigFile();
            Util.WriteLog("Config count: " + l_configData.Count);
            Util.WriteLog("Starting Stress test");

            Factory<TestClient> l_factory = new Factory<TestClient>(TestClient.Initialize);

            m_stressTestReplicator = new StressTestReplicator<TestClient>(l_configData, l_factory);
            Util.WriteLog("Finished Stress test. wait for test to finish");
            Console.ReadKey();
        }

        static Dictionary<string, string> ReadConfigFile()
        {
            Dictionary<string, string> l_config = new Dictionary<string, string>();

            try
            {
                using (StreamReader l_reader = new StreamReader(CONFIG_FILE_NAME))
                {
                    string l_line;
                    while ((l_line = l_reader.ReadLine()) != null)
                    {
                        if (l_line.StartsWith("#") || l_line.StartsWith("//"))
                            continue;

                        string[] l_keyValuePair = l_line.Split("=");
                        l_config.Add(l_keyValuePair[0], l_keyValuePair[1]);
                    }
                }
            }
            catch (FileNotFoundException a_ex)
            {
                Console.WriteLine("File not found" + a_ex);
            }

            return l_config;
        }
    }
}
