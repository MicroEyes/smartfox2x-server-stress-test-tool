﻿using sfs_stress_tool.framework;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace sfs_stress_tool.example
{
    public class TestClient : SimpleClient
    {
        const int MAX_CHAR_COUNT = 5;
        readonly int[] SUPPPORTED_PLATFORMS = new int[] { 1, 2, 11, 8 };


        const string EXT_CMD__CHAR_SELECT = "zone.charSelect";
        const string EXT_CMD__KEEP_ALIVE = "zone.keepAlive";
        const string EXT_CMD__GET_INIT_DATA = "room.initData";

        public const string PACKET_ID = "pkId";
        public const string TRANSFORM__POSITION_X = "px";
        public const string TRANSFORM__POSITION_Y = "py";
        public const string TRANSFORM__POSITION_Z = "pz";

        public const string TRANSFORM__ROTATION_X = "rx";
        public const string TRANSFORM__ROTATION_Y = "ry";
        public const string TRANSFORM__ROTATION_Z = "rz";
        const string AXIS_VALUE_HORIZONTAL = "h";
        public const string AXIS_VALUE_VERTICAL = "v";
        public const string KEY_IS_PLAYER_SPRINTITNG = "isPlayerSprinting";
        public const string EXT_CMD__PLAYER_MOVEMENT_UPDATE = "player.manUpdate";

        public const string SFS_ROOM = "Lobby";

        private Room SFSRoom { get; set; }

        Timer m_timerSendPlayerPosition;
        Timer m_timerSendTextChatMessage;

        public TestClient() : base() { }

        protected override void RegisterSFSCallbacks()
        {
            m_sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnEventReceived);
            m_sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnEventReceived);
        }


        private void StartTimers()
        {
            m_timerSendPlayerPosition = new Timer(16);
            m_timerSendPlayerPosition.Elapsed += TimerSendPlayerPosition_Elapsed;
            m_timerSendPlayerPosition.Start();


            m_timerSendTextChatMessage = new Timer(2000);
            m_timerSendTextChatMessage.Elapsed += TimerSendTextChatMessage_Elapsed;
            m_timerSendTextChatMessage.Start();
        }


        #region SFS Callbacks
        protected override void OnLogin_Post(BaseEvent a_eventData)
        {
            ISFSObject l_sfsObject = new SFSObject();
            l_sfsObject.PutInt("charId", Util.RandomInt(MAX_CHAR_COUNT));
            l_sfsObject.PutUtfString("playerDispName", "StressToolUser-" + Util.RandomInt(1000));
            l_sfsObject.PutInt("p", SUPPPORTED_PLATFORMS[Util.RandomInt(SUPPPORTED_PLATFORMS.Length - 1)]);

            m_sfs.Send(new ExtensionRequest(EXT_CMD__CHAR_SELECT, l_sfsObject));
        }

        protected override void OnExtensionResponse_Post(BaseEvent a_eventData)
        {
            string l_cmd = (string)a_eventData.Params["cmd"];
            ISFSObject l_obj = a_eventData.Params["params"] as SFSObject;

            //Util.WriteLog("Dump: " + l_obj.GetDump(false));
            if (l_cmd.Equals(EXT_CMD__CHAR_SELECT))
            {
                Util.WriteLog("Char Select succeful");
                m_sfs.Send(new JoinRoomRequest(SFS_ROOM));
            }
            else if (l_cmd.Equals(EXT_CMD__GET_INIT_DATA))
            {
                Util.WriteLog("Init data received successful.");
                Util.WriteLog("InitDump:\n" + l_obj.GetDump(false));
                StartTimers();
            }
        }

        protected override void OnRoomJoin_Post(BaseEvent a_eventData)
        {
            SFSRoom = (Room)a_eventData.Params["room"];
            Util.WriteLog("Room Joined: Requesting INIT_DATA.");
            ISFSObject l_sfsObject = new SFSObject();
            m_sfs.Send(new ExtensionRequest(EXT_CMD__GET_INIT_DATA, l_sfsObject, SFSRoom));
        }
        protected override void OnRoomJoinError_Post(BaseEvent a_eventData)
        {
            Destroy();
        }
        #endregion

        int m_packetIndex = 0;
        #region Timer Callbacks
        private void TimerSendPlayerPosition_Elapsed(object sender, ElapsedEventArgs e)
        {
            ISFSObject l_obj = new SFSObject();
            l_obj.PutLong(PACKET_ID, ++m_packetIndex);
            l_obj.PutFloat(AXIS_VALUE_HORIZONTAL, 0.0f);
            l_obj.PutFloat(AXIS_VALUE_VERTICAL, 0.0f);
            l_obj.PutBool(KEY_IS_PLAYER_SPRINTITNG, false);

            l_obj.PutFloat(TRANSFORM__POSITION_X, 0.0f);
            l_obj.PutFloat(TRANSFORM__POSITION_Y, 0.0f);
            l_obj.PutFloat(TRANSFORM__POSITION_Z, 0.0f);

            l_obj.PutFloat(TRANSFORM__ROTATION_X, 0.0f);
            l_obj.PutFloat(TRANSFORM__ROTATION_Y, 0.0f);
            l_obj.PutFloat(TRANSFORM__ROTATION_Z, 0.0f);

            m_sfs.Send(new ExtensionRequest(EXT_CMD__PLAYER_MOVEMENT_UPDATE, l_obj, SFSRoom));
        }

        private const string KEY_MESSAGE = "msg";
        private const string KEY_SFS_ID_PLAYER_RECEIVER = "sfsIdTo";
        private const string EXT_CMD__MESSAGE = "txtChat.msg";
        private void TimerSendTextChatMessage_Elapsed(object sender, ElapsedEventArgs e)
        {
            ISFSObject l_obj = new SFSObject();
            l_obj.PutUtfString(KEY_MESSAGE, "Test message -" + Util.RandomInt(10000));
            l_obj.PutInt(KEY_SFS_ID_PLAYER_RECEIVER, Factory<TestClient>.RandomClient().SFSId);

            Util.WriteLog("Sending text chat request.", m_thread.ManagedThreadId);
            m_sfs.Send(new ExtensionRequest(EXT_CMD__MESSAGE, l_obj, SFSRoom));
        }

        #endregion

    }
}