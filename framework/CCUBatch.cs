﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace sfs_stress_tool.framework
{
    /// <summary>
    /// Handles a batch of client.
    /// </summary>
    /// <typeparam name="CLIENT_TYPE">Client Class Type to handle.</typeparam>
    public class CCUBatch<CLIENT_TYPE> where CLIENT_TYPE : SimpleClientBase, new()
    {

        /// <summary>
        /// Configurations.
        /// </summary>
        readonly IReadOnlyDictionary<string, string> m_config;

        /// <summary>
        /// Thread responsible of instantiating Child objects.
        /// </summary>
        Thread m_thread;

        /// <summary>
        /// List of clients.
        /// </summary>
        List<CLIENT_TYPE> m_lstClients = new List<CLIENT_TYPE>();

        /// <summary>
        /// List of clients (ReadOnly).
        /// </summary>
        public IReadOnlyList<CLIENT_TYPE> Clients
        {
            get { return m_lstClients; }
        }

        /// <summary>
        /// Count of client's spawned successfully.
        /// </summary>
        static int m_clientSpawnCount = 0;

        /// <summary>
        /// Factory reference used to create objects.
        /// </summary>
        Factory<CLIENT_TYPE> m_factory;
        public CCUBatch(IReadOnlyDictionary<string, string> a_config, Factory<CLIENT_TYPE> a_factory)
        {
            Util.WriteLog("CCU Batch");
            m_config = a_config;
            m_factory = a_factory;
        }

        /// <summary>
        /// Spawns thread for this instance.
        /// </summary>
        public void SpawnThread()
        {
            //Creating seperate Thread to instantiate Client batch to not to block parent thread.
            ThreadStart l_threadCCUBatch = new ThreadStart(StartBatch);
            m_thread = new Thread(l_threadCCUBatch);
            m_thread.Start();
        }


        /// <summary>
        /// Start spawning batch of client.
        /// </summary>
        private void StartBatch()
        {
            Util.WriteLog("Staring generation of clients");
            string l_strCCUPerBatch = m_config["batch.CCUPerBatch"];

            int l_CCUPerBatch;
            string[] l_arrCCUPerBatch;

            //Checking if configuration contains a Range Type.
            if ((l_arrCCUPerBatch = l_strCCUPerBatch.Split("-")).Length == 2)
            {
                int l_CCUPerBatchMin = int.Parse(l_arrCCUPerBatch[0]);
                int l_CCUPerBatchMax = int.Parse(l_arrCCUPerBatch[1]);

                Random l_randomCCU = new Random((int)DateTime.Now.Ticks);
                l_CCUPerBatch = l_randomCCU.Next(l_CCUPerBatchMin, l_CCUPerBatchMax);
            }
            else         //if configuration contains Integer type.
            {
                l_CCUPerBatch = int.Parse(l_strCCUPerBatch);
            }
            Util.WriteLog("Spawning client count: " + l_CCUPerBatch);
            for (int l_index = 0; l_index < l_CCUPerBatch; l_index++)
            {
                CLIENT_TYPE l_client = m_factory.CreateClient(Interlocked.Increment(ref m_clientSpawnCount), m_config);
                m_lstClients.Add(l_client);
                l_client.Start();
            }
        }
    }
}
