﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sfs_stress_tool.framework
{   
    /// <summary>
    /// Factory used to create objects of a type.
    /// </summary>
    /// <typeparam name="CLIENT_TYPE">Client type.</typeparam>
    public class Factory<CLIENT_TYPE> where CLIENT_TYPE : SimpleClientBase, new()
    {
        /// <summary>
        /// Delegate type used to instantiate object after creating.
        /// </summary>
        /// <param name="a_client">Client instance to instantiate</param>
        /// <param name="a_index">Client index in 'CCUBatch'</param>
        /// <param name="a_config">Configuration</param>
        public delegate void InitializationMethod(CLIENT_TYPE a_client, int a_index, IReadOnlyDictionary<string, string> a_config);

        /// <summary>
        /// Method reference to instantiate object after creating.
        /// </summary>
        private InitializationMethod m_clientInitializeMethods;

        /// <summary>
        /// Client count created by Factory<CLIENT_TYPE>.
        /// </summary>
        public static int ClientCount { get { return m_lstClients.Count; } }

        /// <summary>
        /// List of all clients created by Factory<CLIENT_TYPE>.
        /// </summary>
        public static List<CLIENT_TYPE> m_lstClients = new List<CLIENT_TYPE>();

        public Factory(InitializationMethod a_clientInitializeMethods)
        {
            m_clientInitializeMethods = a_clientInitializeMethods;
        }

        /// <summary>
        /// Creates a client.
        /// </summary>
        /// <param name="a_index">Index in 'CCUBatch'</param>
        /// <param name="a_config">Configuration.</param>
        /// <returns></returns>
        public CLIENT_TYPE CreateClient(int a_index, IReadOnlyDictionary<string, string> a_config)
        {
            CLIENT_TYPE l_client = new CLIENT_TYPE();
            m_clientInitializeMethods(l_client, a_index, a_config);
            m_lstClients.Add(l_client);
            return l_client;
        }

        /// <summary>
        /// Destroys a client.
        /// </summary>
        /// <param name="a_client">Client to destroy.</param>
        public void DestroyClient(CLIENT_TYPE a_client)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns a random client reference.
        /// </summary>
        /// <returns></returns>
        public static CLIENT_TYPE RandomClient()
        {
            return m_lstClients[Util.RandomInt(m_lstClients.Count - 1)];
        }
    }
}
