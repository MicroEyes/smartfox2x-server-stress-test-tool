﻿using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace sfs_stress_tool.framework
{
    #region Base implementation

    /// <summary>
    /// Base minimal implementation of a client.
    /// </summary>
    public abstract class SimpleClientBase
    {
        /// <summary>
        /// Client index.
        /// </summary>
        public abstract int ClientIndex { get; protected set; }

        /// <summary>
        /// Smartfox Id.
        /// </summary>
        public abstract int SFSId { get; }

        /// <summary>
        /// Configuration.
        /// </summary>
        public abstract IReadOnlyDictionary<string, string> Config { get; protected set; }

        /// <summary>
        /// Start initializing & executing the client.
        /// </summary>
        public abstract void Start();
    }
    #endregion

    /// <summary>
    /// Minimal implementation of a client.
    /// </summary>
    public class SimpleClient : SimpleClientBase
    {
        /// <summary>
        /// Client index.
        /// </summary>
        public override int ClientIndex { get; protected set; }

        /// <summary>
        /// Smartfox Id.
        /// </summary>
        public override int SFSId { get { return m_sfs.MySelf.Id; } }

        /// <summary>
        /// Configuration.
        /// </summary>
        public override IReadOnlyDictionary<string, string> Config { get; protected set; }

        /// <summary>
        /// Smartfox object reference.
        /// </summary>
        protected SmartFox m_sfs;

        /// <summary>
        /// Thread reference of this object.
        /// </summary>
        protected Thread m_thread;

        /// <summary>
        /// Test account username format.
        /// </summary>
        const string FORMAT_USERNAME = "____testUser{0}";

        /// <summary>
        /// Test account password format.
        /// </summary>
        const string FORMAT_PASSWORD = "____testPassword{0}";        

        public SimpleClient() { }

        /// <summary>
        /// Initializes a client.
        /// </summary>
        /// <param name="a_client"></param>
        /// <param name="a_index"></param>
        /// <param name="a_config"></param>
        public static void Initialize(SimpleClient a_client, int a_index, IReadOnlyDictionary<string, string> a_config)
        {
            a_client.ClientIndex = a_index;
            a_client.Config = a_config;
        }


        /// <summary>
        /// Start initializing & executing the client on a new Thread.
        /// </summary>
        public override void Start()
        {
            m_thread = new Thread(new ThreadStart(PerformStartup));
            m_thread.Start();
        }

        /// <summary>
        /// Destroy and clean of this object.
        /// </summary>
        public void Destroy()
        {
            if (m_sfs != null)
            {
                m_sfs.RemoveAllEventListeners();
                m_sfs.Disconnect();
            }


            if (m_thread != null && m_thread.IsAlive)
                m_thread.Abort();
        }

        /// <summary>
        /// Starts initialization and execution of SFS.
        /// </summary>
        private void PerformStartup()
        {
            Util.WriteLog("Configuring sfs instance");
            m_sfs = new SmartFox();
            ConfigData m_sfsConfigData = new ConfigData
            {
                Host = Config["sfs.host"],
                Port = int.Parse(Config["sfs.port"]),
                Zone = Config["sfs.zone"],
                Debug = bool.Parse(Config["sfs.debug"])
            };

            Internal_RegisterSFSCallbacks();

            Util.WriteLog("Connecting to server.");
            m_sfs.Connect(m_sfsConfigData);

            while (m_thread.IsAlive)
            {
                Thread.Sleep(16);
                m_sfs.ProcessEvents();
            }
        }

        /// <summary>
        /// Minimal SFS callbacks to register.
        /// </summary>
        private void Internal_RegisterSFSCallbacks()
        {
            m_sfs.AddEventListener(SFSEvent.CONNECTION, OnEventReceived);
            m_sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnEventReceived);
            m_sfs.AddEventListener(SFSEvent.LOGIN, OnEventReceived);
            m_sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnEventReceived);
            m_sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnEventReceived);
            RegisterSFSCallbacks();
        }

        /// <summary>
        /// SFS events to register for derived class.
        /// </summary>
        protected virtual void RegisterSFSCallbacks() { }

        /// <summary>
        /// Common function to handle all SFS event callbacks.
        /// </summary>
        /// <param name="a_eventData">SFS Event data</param>
        protected virtual void OnEventReceived(BaseEvent a_eventData)
        {
            string l_eventType = a_eventData.Type;
            IDictionary<string, object> l_data = a_eventData.Params;

            if (l_eventType.Equals(SFSEvent.CONNECTION))
            {
                Util.WriteLog("Connection established.");
                m_sfs.Send(new LoginRequest(string.Format(FORMAT_USERNAME, ClientIndex), string.Format(FORMAT_PASSWORD, ClientIndex)));
            }
            else if (l_eventType.Equals(SFSEvent.CONNECTION_LOST))
            {
                Util.WriteLog("Disconnected.");
            }
            else if (l_eventType.Equals(SFSEvent.LOGIN))
            {
                Util.WriteLog("logged in successfully.");

                OnLogin_Post(a_eventData);
            }
            else if (l_eventType.Equals(SFSEvent.LOGIN_ERROR))
            {
                Util.WriteLog("logged error.");
            }
            else if(l_eventType.Equals(SFSEvent.EXTENSION_RESPONSE))
            {
                OnExtensionResponse_Post(a_eventData);
            }
            else if(l_eventType.Equals(SFSEvent.ROOM_JOIN))
            {
                Util.WriteLog("Room joined.");
                OnRoomJoin_Post(a_eventData);
            }
            else if (l_eventType.Equals(SFSEvent.ROOM_JOIN_ERROR))
            {
                Util.WriteLog("Room error.");
                OnRoomJoinError_Post(a_eventData);
            }

        }

        #region Protected SFS Callbacks for derived class
        /// <summary>
        /// Callback on successful login.
        /// </summary>
        /// <param name="a_eventData"></param>
        protected virtual void OnLogin_Post(BaseEvent a_eventData) { }

        /// <summary>
        /// Callback on receiving extension response.
        /// </summary>
        /// <param name="a_eventData"></param>
        protected virtual void OnExtensionResponse_Post(BaseEvent a_eventData) { }

        /// <summary>
        /// Callback on successful room join.
        /// </summary>
        /// <param name="a_eventData"></param>
        protected virtual void OnRoomJoin_Post(BaseEvent a_eventData) { }

        /// <summary>
        /// Callback on room join error.
        /// </summary>
        /// <param name="a_eventData"></param>
        protected virtual void OnRoomJoinError_Post(BaseEvent a_eventData) { }
        #endregion
    }
}
