﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace sfs_stress_tool.framework
{
    /// <summary>
    /// Responsible for starting the test. Handles 'CCUBatch' objects.
    /// </summary>
    /// <typeparam name="CLIENT_TYPE">Client Type</typeparam>
    public class StressTestReplicator<CLIENT_TYPE> where CLIENT_TYPE : SimpleClientBase, new()
    {
        /// <summary>
        /// Configuration.
        /// </summary>
        readonly IReadOnlyDictionary<string, string> m_config;

        /// <summary>
        /// Thread used to create 'CCUBatch'.
        /// </summary>
        Thread m_mainThread;

        /// <summary>
        /// Contains List batch.
        /// </summary>
        List<CCUBatch<CLIENT_TYPE>> m_lstThreadBatch = new List<CCUBatch<CLIENT_TYPE>>();

        /// <summary>
        /// Factory for client.
        /// </summary>
        Factory<CLIENT_TYPE> m_factory;

        /// <summary>
        /// Cummulative client count.
        /// </summary>
        public int ClientCount
        {
            get 
            {
                int l_count = 0;
                for (int l_index = 0; l_index < m_lstThreadBatch.Count; l_index++)
                {
                    l_count += m_lstThreadBatch[l_index].Clients.Count;
                }

                return l_count;
            }
        }

        public StressTestReplicator(IReadOnlyDictionary<string, string> a_config, Factory<CLIENT_TYPE> a_factory)
        {
            m_config = a_config;
            m_factory = a_factory;

            m_mainThread = new Thread(new ThreadStart(StartBatchGeneration));
            m_mainThread.Start();
        }

        /// <summary>
        /// Starts creating batches.
        /// </summary>
        public void StartBatchGeneration()
        {
            Util.WriteLog("Starting batch generations on threadId ");
            int l_totalCCUBatchCount = int.Parse(m_config["main.totalCCUBatch"]);
            int l_generationDelayPerBatch = int.Parse(m_config["main.generationDelayPerBatch"]);

            for (int l_index = 0; l_index < l_totalCCUBatchCount; l_index++)
            {
                Util.WriteLog("Starting batch index " + l_index + " with time: " + l_generationDelayPerBatch);
                CCUBatch<CLIENT_TYPE> l_ccuBatch = new CCUBatch<CLIENT_TYPE>(m_config, m_factory);                
                m_lstThreadBatch.Add(l_ccuBatch);
                l_ccuBatch.SpawnThread();
                Thread.Sleep(l_generationDelayPerBatch);
            }
            Util.WriteLog("Batch generation finished");
        }
    }
}
