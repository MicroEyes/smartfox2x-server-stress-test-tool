﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace sfs_stress_tool.framework
{
    public class Util
    {
        /// <summary>
        /// Writes log to console.
        /// </summary>
        /// <param name="a_msg">Message</param>
        public static void WriteLog(string a_msg)
        {
            Console.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + ":[" + Thread.CurrentThread.ManagedThreadId + "]:" + a_msg);
        }

        /// <summary>
        /// Writes log to console.
        /// </summary>
        /// <param name="a_msg">Message</param>
        /// <param name="a_threadId">ThreadId to append.</param>
        public static void WriteLog(string a_msg, int a_threadId)
        {
            Console.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + ":[" + a_threadId + ":" + Thread.CurrentThread.ManagedThreadId + "]:" + a_msg);
        }

        static readonly Random s_random = new Random();
        
        /// <summary>
        /// Returns a random number.
        /// </summary>
        /// <returns></returns>
        public static int RandomInt()
        {
            return s_random.Next();
        }

        /// <summary>
        /// Returns a non-negative random number including maxNumber.
        /// </summary>
        /// <param name="a_maxNumber">Max number.</param>
        /// <returns></returns>
        public static int RandomInt(int a_maxNumber)
        {
            return s_random.Next(a_maxNumber);
        }
    }
}
